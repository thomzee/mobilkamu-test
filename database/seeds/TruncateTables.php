<?php

use Illuminate\Database\Seeder;

class TruncateTables extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement("SET foreign_key_checks=0");
        \App\Models\Product::truncate();
        DB::statement("SET foreign_key_checks=1");
    }
}
