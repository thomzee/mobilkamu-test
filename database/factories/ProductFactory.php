<?php

use App\User;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(\App\Models\Product::class, function (Faker $faker) {
    $fuels = [
       'gasoline', 'diesel', 'hydrogen', 'electric'
    ];

    return [
        'brand' => $faker->company,
        'model' => $faker->lastName,
        'fuel' => $fuels[rand(0, sizeof($fuels)-1)],
        'price' => rand(150000000, 600000000),
    ];
});
