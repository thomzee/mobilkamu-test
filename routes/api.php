<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['as' => 'api::', 'namespace' => 'Api', 'prefix' => 'v1'], function() {
    Route::group(['as' => 'product.', 'prefix' => 'product'], function() {
        Route::get('/', ['as' => 'index', 'uses' => 'ProductController@index']);
        Route::get('/show/{id}', ['as' => 'show', 'uses' => 'ProductController@show']);
        Route::post('/create', ['as' => 'store', 'uses' => 'ProductController@store']);
        Route::post('/comment/{id}', ['as' => 'comment', 'uses' => 'ProductController@comment']);
        Route::post('/comment/{id}/update-point', ['as' => 'updatePoint', 'uses' => 'ProductController@updatePoint']);
    });
});
