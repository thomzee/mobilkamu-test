<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['brand', 'model', 'price', 'fuel', 'image_id'];

    public function image() {
        return $this->belongsTo(Image::class);
    }

    public function comments() {
        return $this->hasMany(Comment::class);
    }
}
