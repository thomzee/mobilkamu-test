<?php
/**
 * Created by PhpStorm.
 * User: thomzee
 * Date: 25/04/2019
 * Time: 23:04
 */

namespace App\Libraries;


use Illuminate\Http\JsonResponse;

class ResponseLibrary
{
    public function success() {
        $return = [];
        $return['meta']['code'] = JsonResponse::HTTP_OK;
        $return['meta']['message'] = trans('message.api.success');
        return $return;
    }

    public function error(\Exception $e) {
        $return = [];
        $return['meta']['code'] = JsonResponse::HTTP_INTERNAL_SERVER_ERROR;
        $return['meta']['message'] = trans('message.api.error');
        $return['meta']['error'] = $e->getMessage();
        return $return;
    }
}
