<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        $this->routes = [
            'admin'   => 'admin::',
            'api'   => 'api::',
        ];
    }

    public function share()
    {
        view()->share([
            'menu'  => $this->menu,
            'route' => $this->route,
            'view'  => $this->view
        ]);
    }
}
