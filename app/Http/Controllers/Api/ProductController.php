<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\StoreComment;
use App\Http\Requests\StoreProduct;
use App\Libraries\ResponseLibrary;
use App\Models\Comment;
use App\Models\Image;
use App\Models\Product;
use App\Responses\ProductResponse;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    private $model;
    private $image;
    private $responseLib;
    private $response;
    private $comment;

    public function __construct()
    {
        parent::__construct();
        $this->model = new Product();
        $this->responseLib = new ResponseLibrary();
        $this->response = new ProductResponse();
        $this->image = new Image();
        $this->comment = new Comment();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $data = $this->model;
            return response(
                $this->response->paginated(
                    $data,
                    request()->get('per_page'),
                    request()->get('sort')
                ),JsonResponse::HTTP_OK
            );
        } catch (\Exception $e) {
            return response($this->responseLib->error($e), JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProduct $request)
    {
        try {
            DB::beginTransaction();

            $product = $this->model;

            if ($request->hasFile('file')) {
                if ($request->file('file')->isValid()) {
                    $dir = check_dir('images/products');
                    $extension = $request->file('file')->getClientOriginalExtension(); // getting image extension
                    $filename = time().'.'.$extension;
                    $request->file('file')->move($dir, $filename);

                    $image_id = $this->image->create(['image' => $dir.'/'.$filename])->id;
                    $product->image_id = $image_id;
                }
            }

            $product->model = $request->model;
            $product->brand = $request->brand;
            $product->fuel = $request->fuel;
            $product->price = $request->price;
            $product->save();
            DB::commit();
            return response()->json($this->response->singleData($product), JsonResponse::HTTP_OK);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json($this->responseLib->error($e), JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $product = $this->model
                ->with(['comments' => function ($q) {
                    $q->orderBy('created_at', 'desc');
                }])
                ->findOrFail($id);
            return response()->json($this->response->singleData($product), JsonResponse::HTTP_OK);
        } catch (\Exception $e) {
            return response()->json($this->responseLib->error($e), JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function comment(StoreComment $request, $id)
    {
        try {
            DB::beginTransaction();

            $product = $this->model->findOrFail($id);
            $product->comments()->create($request->all());
            DB::commit();
            return response()->json($this->response->singleData($product), JsonResponse::HTTP_OK);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json($this->responseLib->error($e), JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function updatePoint(Request $request, $id)
    {
        try {
            DB::beginTransaction();

            $comment = $this->comment->findOrFail($id);
            $currentPoint = $comment->point;
            $comment->point = $currentPoint+$request->point;
            $comment->save();
            DB::commit();
            return response()->json($this->responseLib->success(), JsonResponse::HTTP_OK);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json($this->responseLib->error($e), JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
