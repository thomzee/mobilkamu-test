<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreProduct extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'brand' => 'required|max:255',
            'model' => 'required|max:255',
            'fuel' => 'required|in:electric,diesel,gasoline,hydrogen',
            'price' => 'required|numeric|min:1',
            'file' => 'required|mimes:jpg,png,jpeg',
        ];
    }
}
