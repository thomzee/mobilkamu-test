<?php
/**
 * Created by PhpStorm.
 * User: thomzee
 * Date: 26/04/2019
 * Time: 0:12
 */

namespace App\Responses;


use Illuminate\Http\JsonResponse;

class ProductResponse
{
    public function paginated($collection, $perPage = null, $sort = null)
    {
        $return = [];
        $paginated = $collection;

        if ($sort) {
            $exp = explode('|', $sort);
            $paginated = $paginated->orderBy($exp[0], $exp[1]);
        } else {
            $paginated = $paginated->orderBy('created_at', 'asc');
        }

        if ($perPage) {
            $paginated = $paginated->paginate($perPage);
        } else {
            $paginated = $paginated->paginate(10);
        }

        $return['meta']['code'] = JsonResponse::HTTP_OK;
        $return['meta']['message'] = trans('message.api.success');
        $return['links']['pagination']['total'] = $paginated->total();
        $return['links']['pagination']['per_page'] = $paginated->perPage();
        $return['links']['pagination']['current_page'] = $paginated->currentPage();
        $return['links']['pagination']['last_page'] = $paginated->lastPage();
        $return['links']['pagination']['has_more_pages'] = $paginated->hasMorePages();
        $return['links']['pagination']['from'] = $paginated->firstItem();
        $return['links']['pagination']['to'] = $paginated->lastItem();
        $return['links']['pagination']['self'] = url()->full();
        $return['links']['pagination']['next_page_url'] = $paginated->nextPageUrl();
        $return['links']['pagination']['prev_page_url'] = $paginated->previousPageUrl();

        foreach ($paginated->items() as $item) {
            $return['data'][] = $this->mapper($item);
        }
        return $return;
    }

    private function mapper($data) {
        $result = $data;
        return $result;
    }

    public function singleData($data) {
        $return['meta']['code'] = JsonResponse::HTTP_OK;
        $return['meta']['message'] = trans('message.api.success');
        $return['data'] = $data;
        $return['data']['file'] = asset($data->image->image);
        return $return;
    }
}
