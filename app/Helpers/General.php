<?php

if (!function_exists('numrows')) {
    function numrows($data, $index = 1, $name = 'no')
    {
        if (is_object($data)) {
            foreach ($data as $key => $value) {
                $value->{$name} = $index;
                $index++;
            }
        } else
            if (is_array($data)) {
                foreach ($data as $key => $value) {
                    $data[$key][$name] = $index;
                    $index++;
                }
            }
        return $data;
    }
}

if (! function_exists('check_dir')) {
    function check_dir($pathname)
    {
        if (!is_dir($pathname)) {
            mkdir($pathname, 0777, true);
        }
        return $pathname;
    }
}
