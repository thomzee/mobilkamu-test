@extends('layouts.app')
@section('title', $menu.' | ')
@section('content')
    <div class="module">
        <div class="row">
            <div class="col-md-12">
                <a href="javascript:void(0);" class="btn btn-primary">{!! trans('button.create') !!}</a>
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">{{ $menu }}</h3>
                    </div>
                    <div class="box-body">
                        <table class="table table-bordered table-datatables">
                            <thead>
                                <tr>
                                    <th scope="col">No</th>
                                    <th scope="col">Brand</th>
                                    <th scope="col">Model</th>
                                    <th scope="col">Fuel</th>
                                    <th scope="col">Price</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    @include($view.'.scripts')
@endpush
