<script type="text/javascript">
    $(function() {
        $('.table-datatables').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route($route.".datatables") !!}',
            columns: [
                {data: 'no', searchable: false, width: '5%', className: 'center'},
                {data: 'brand'},
                {data: 'model'},
                {data: 'fuel'},
                {data: 'price'},
                {data: 'action', orderable: false, searchable: false, width: '15%', className: 'center action'},
            ]
        });
    });
</script>
