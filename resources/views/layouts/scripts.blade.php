<!-- Global -->
<script src="https://cdn.jsdelivr.net/npm/es6-promise@4/dist/es6-promise.auto.js"></script>
<script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/vuex-3.1.0/vuex.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-3.3.1/jquery-3.3.1.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/bootstrap-4.0.0-dist/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/fontawesome-free-5.8.1-web/js/all.min.js') }}"></script>

@yield('scripts')
@stack('scripts')
