<!-- Global -->
<link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/bootstrap-4.0.0-dist/css/bootstrap.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/fontawesome-free-5.8.1-web/css/all.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('customs/css/styles.css') }}">

@yield('styles')
@stack('styles')
