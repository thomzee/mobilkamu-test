<footer class="main-footer">
    <strong>Copyright © {{ date('Y') . ' ' . trans('app.title') }}.</strong>
</footer>
