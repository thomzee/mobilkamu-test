@extends('layouts.app')
@section('title', $menu.' | ')
@section('content')
    <div class="module">
        <div class="row">
            <div class="col-md-12">
                {{--<a href="javascript:void(0);" class="btn btn-primary"><i class="fa fa-plus"></i> Create</a>--}}
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">{{ $menu }}</h3>
                    </div>
                    <div class="box-body">
                        <div id="app">
                            <router-view></router-view>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
