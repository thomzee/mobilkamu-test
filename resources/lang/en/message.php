<?php

return [

    'api' => [
        'success' => 'Operation successfully executed.',
        'error' => 'Operation failed.',
    ],
];
